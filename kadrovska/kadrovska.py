from osv import fields,osv

class odjel(osv.osv):

    _name = 'odjel'
    _rec_name = "odjel"
    _columns = {
        'odjel': fields.char('Odjel',size=30, required=True, help='odjel'),
        
    }  
    #@staticmethod
    '''def _dohvati_odjele(self, cr, uid, ids, context = None):
        res=[]
        cr.execute('select id , odjel from odjel order by odjel asc')
        odjeli=cr.fetchall()
        for odjel in odjeli:
            res.append((odjel.id,odjel.odjel))
        return res'''
    
odjel()


class djelatnik(osv.osv):

    
    #_inherit= 'odjel'
    _name = 'djelatnik'

    _columns = {
        'ime': fields.char('Ime',size=30, required=True, help='ime'),
        'prezime': fields.char('Prezime', size=30, required=True, help='prezime'),
        #'odjel':fields.selection([('informatika','Informatika'),('prodaja','Prodaja'),('marketing','Marketing'),('odrzavanje','Odrzavanje'),('marketing','Marketing'), ('uprava','Uprava')],'Odjel', required=True),
        'datum_rodjenja': fields.date('Datum rodjenja', size=30, required=True, help='datum rodjenja'),
        'email': fields.char('Email', size=50, help='email'),
        'telefon': fields.integer('Telefon', size=30, help='telefon'),    
        'status':fields.selection([('aktivan','Aktivan'),('neaktivan','Neaktivan')],'Status', required=True),
        'odjel':fields.many2one('odjel', 'Odjel'),
        
    }	
    
    
djelatnik()






